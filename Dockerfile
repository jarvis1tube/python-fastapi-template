FROM python:3.9-slim-buster as app

WORKDIR /opt/mnt

COPY poetry.toml .
COPY pyproject.toml .
COPY poetry.lock .

RUN pip3 install --upgrade pip
RUN pip3 install poetry
RUN poetry install --no-dev
ENV PATH="/opt/mnt/.venv/bin:$PATH"

COPY . .

FROM app as test

RUN poetry install

from fastapi import FastAPI
from fastapi.responses import JSONResponse

app = FastAPI()


@app.get("/hello")
def start_page():
    """My hello world page."""
    return JSONResponse(
        content={
            "message": "Hello world!",
        }
    )
